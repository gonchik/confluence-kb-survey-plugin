Confluence KB Survey plugin
===========================

Building
--------
`mvn clean install -DskipTests -Dproduct=confluence`

Running
-------

1. `mvn amps:run -Dproduct=confluence` brings up Confluence with the required plugins
2. configure the plugin in the space admin per space

Running all tests locally
--------
`mvn clean install` with Maven 3 and Java 7

Release Procedure
-----------------
Use appropriate build on EBAC: https://bamboo.extranet.atlassian.com/browse/CKBS-KSMVC5