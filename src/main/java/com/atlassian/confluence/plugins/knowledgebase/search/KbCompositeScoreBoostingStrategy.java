package com.atlassian.confluence.plugins.knowledgebase.search;

import static com.atlassian.confluence.plugins.knowledgebase.search.KbSearchExtractor.COMPOSITE_SCORE_FIELD_KEY;
import static com.google.common.base.Strings.emptyToNull;
import static java.util.Collections.singleton;

import com.atlassian.confluence.plugins.knowledgebase.SurveySettingsManager;
import com.atlassian.confluence.search.service.SearchQueryParameters;
import com.atlassian.confluence.search.v2.lucene.boosting.BoostingStrategy;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;

import java.io.IOException;

public class KbCompositeScoreBoostingStrategy implements BoostingStrategy
{
    private static final Logger log = Logger.getLogger(KbCompositeScoreBoostingStrategy.class);
    private SurveySettingsManager surveySettingsManager;

    public KbCompositeScoreBoostingStrategy(SurveySettingsManager surveySettingsManager)
    {
        this.surveySettingsManager = surveySettingsManager;
    }

    public float boost(IndexReader reader, int doc, float score) throws IOException
    {
        final String compositeScoreString = lookupCompositeScore(reader, doc, score);
        if (compositeScoreString!=null&&!compositeScoreString.equals("0"))
        {
            Float compositeScore = Float.parseFloat(compositeScoreString);
            if (log.isDebugEnabled())
               log.debug("Composite Score from Index: " + compositeScore);
            if (compositeScore > surveySettingsManager.getBoostMaximum() || compositeScore < surveySettingsManager.getBoostMinimum())
            {
              compositeScore = surveySettingsManager.getBoostMaximum().floatValue();
            }

            if (log.isDebugEnabled())
               log.debug("Composite Score after boosting: " + compositeScore);

            if (compositeScore!=0)
              score *= (1+(compositeScore*(surveySettingsManager.getBoostAmplifier()*.01)));

            if (log.isDebugEnabled())
              log.debug("Final Lucene score after boosting and amplification: " + score);
        }
        return score;
    }

    public float boost(org.apache.lucene.index.IndexReader p0, java.util.Map<java.lang.String,java.lang.Object> p1, int p2, float p3) throws java.io.IOException
    {
            throw new IOException("KbCompositeScoreBoostingStrategy should not call this method");
    }
    String lookupCompositeScore(IndexReader reader, int doc, float score)
            throws IOException
    {
        if (log.isDebugEnabled())
        {
            log.debug("Lucene score before Boosting: " +
                    "SpaceKey= " + reader.document(doc).getField("spacekey") + ", " +
                    "Title = " + reader.document(doc).getField("title") + ": " + score);
            log.debug("fieldcaches.length: " + reader.getDocCount(COMPOSITE_SCORE_FIELD_KEY));
        }
        // better ranked pages get a boost
        Document document = reader.document(doc, singleton(COMPOSITE_SCORE_FIELD_KEY));
        return emptyToNull(document.get(COMPOSITE_SCORE_FIELD_KEY));
    }

    public float boost(IndexReader reader, SearchQueryParameters searchQueryParameters, int doc, float score) throws IOException
    {
        return boost(reader, doc, score);
    }
}
