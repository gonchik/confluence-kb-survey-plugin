package com.atlassian.confluence.plugins.knowledgebase.search;

import com.atlassian.bonnie.Searchable;
import com.atlassian.bonnie.search.Extractor;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;

public class KbSearchExtractor implements Extractor{
    public static final String COMPOSITE_SCORE_FIELD_KEY = "compositeScore";

    private SurveyFeedbackManager surveyFeedbackManager;

    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }

    public void addFields(Document document, StringBuffer not_used, Searchable searchable)
    {

        if (!(searchable instanceof ContentEntityObject))
        {
            return;
        }

        ContentEntityObject content = (ContentEntityObject) searchable;
        if (StringUtils.isBlank(content.getTitle()))
        {
            return;
        }

        if (content instanceof Page)
        {
            String compositeScore = surveyFeedbackManager.getCompositeScore((Page) content);
            final Field field = new StringField(
                    COMPOSITE_SCORE_FIELD_KEY, compositeScore, Field.Store.YES);
            document.add(field);
        }

    }

}
