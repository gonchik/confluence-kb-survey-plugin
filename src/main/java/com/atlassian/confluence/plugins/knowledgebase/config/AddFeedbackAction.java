package com.atlassian.confluence.plugins.knowledgebase.config;

import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAdminAction;
import com.atlassian.confluence.spaces.actions.SpaceAware;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.opensymphony.webwork.interceptor.ServletRequestAware;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
@WebSudoRequired
public class AddFeedbackAction extends AbstractSpaceAdminAction implements ServletRequestAware,SpaceAware
{

    private SurveyConfig surveyConfig;
    private String newQuestion;
    private String primaryQuestion;
    private String response;
    private String responseUser;
    private HttpServletRequest request;
    private SurveyFeedbackManager surveyFeedbackManager;

    public String execute() throws Exception
    {
        SpaceDescription spaceDesc = this.getSpace().getDescription();
        surveyConfig =  surveyFeedbackManager.getSurveyConfig(spaceDesc);
        if (StringUtils.isNotBlank(primaryQuestion))
        {
            surveyConfig.setPrimaryQuestionId(this.getSpace().getKey()+"0");
            surveyConfig.setPrimaryQuestion(primaryQuestion);
        }
        if (StringUtils.isNotBlank(newQuestion))
            surveyConfig.addQuestion(new SurveyQuestion(newQuestion,this.getSpace().getKey() + (surveyConfig.getQuestionMap().size()+1)));

        surveyConfig.setResponse(response);
        surveyConfig.setResponseUser(responseUser);


        //Edit names - only for active questions!
        for (SurveyQuestion question : surveyConfig.getActiveQuestionList())
        {
            String newName = request.getParameter(question.getId());
            if (StringUtils.isNotBlank(newName))
            {
              question.setQuestion(newName);
                
              surveyConfig.getQuestionMap().put(question.getId(),question);
            }
        }
        surveyFeedbackManager.setSurveyConfig(spaceDesc,surveyConfig);
        return SUCCESS;

    }

    public void setResponse(String r)
    {
       response = r;
    }

    public String getResponse()
    {
        return response;
    }

    public String getResponseUser()
    {
        return responseUser;
    }

    public void setResponseUser(String responseUser)
    {
        this.responseUser = responseUser;
    }

    public void setAdditionalQuestion(String q)
    {
        newQuestion = q;
    }

    public void setPrimaryQuestion(String pq)
    {
        primaryQuestion = pq;
    }


    public SurveyConfig getSurveyConfig()
    {
        return surveyConfig;
    }

    public void setServletRequest(HttpServletRequest httpServletRequest)
    {
        request = httpServletRequest;
    }

    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }

}