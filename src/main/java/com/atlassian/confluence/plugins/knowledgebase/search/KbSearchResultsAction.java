package com.atlassian.confluence.plugins.knowledgebase.search;

import com.atlassian.confluence.plugins.knowledgebase.SurveySettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.SpaceAware;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.core.*;
import com.atlassian.confluence.search.service.SearchQueryParameters;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.*;
import com.atlassian.confluence.search.v2.filter.SubsetResultFilter;
import com.atlassian.confluence.search.v2.searchfilter.SiteSearchPermissionsSearchFilter;
import com.atlassian.confluence.search.v2.query.*;
import com.atlassian.confluence.search.v2.lucene.*;
import com.atlassian.confluence.search.actions.SearchSiteAction;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.themes.ThemeManager;
import com.atlassian.confluence.web.context.HttpContext;
import com.opensymphony.webwork.interceptor.ServletRequestAware;
import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.Filter;

import com.atlassian.confluence.search.v2.lucene.boosting.BoostingQuery;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import bucket.core.actions.PaginationSupport;

public class KbSearchResultsAction extends SearchSiteAction implements ServletRequestAware, SpaceAware
{
    private static Logger log = Logger.getLogger(KbSearchResultsAction.class);
    private static final Integer MAX_RESULTS_PER_PAGE = 10;
    private static final Integer OVERALL_MAX = 300;

    private PageManager pageManager;
    private SpaceManager spaceManager;
    private ThemeManager themeManager;
    private ConfluenceActionSupport confluenceActionSupport; /* For i18n */
    private SearchManager searchManager;
    private PaginationSupport paginationSupport;
    private HttpServletRequest request;
    private LuceneSearcher searcher;
    private Set<String> spaceKeySet = new HashSet<String>();
    private String queryString;
    private String typeString;
    private String spacesString;
    private String where;
    private String[] spaces;
    private int startIndex;
    private List<SearchResult> sortedSearchResults = new LinkedList<SearchResult>();
    private List<SearchResult> sortedSubList = new LinkedList<SearchResult>();
    private SearchResults searchResults;
    private String[] types;
    protected Space space;
    private String spaceTitles;
    private LuceneSearchMapper searchMapper;
    private SurveySettingsManager surveySettingsManager;
    private HttpContext httpContext;


    public void setSurveySettingsManager(SurveySettingsManager surveySettingsManager)
    {
        this.surveySettingsManager = surveySettingsManager;
    }

    public String getTypeString()
    {
        return typeString;
    }

    public String getSpacesString()
    {
        StringBuilder sb = new StringBuilder();
        for (String sp : spaceKeySet)
        {
            sb.append("spaces=");
            sb.append(sp);
            sb.append("&");
        }
        spacesString = sb.toString();
        return spacesString;
    }

    public void setSpaces(String[] spaces)
    {
        this.spaces = spaces;
    }

    public Space getSpace()
    {
        return space;
    }

    public void setSpace(Space space)
    {
        this.space = space;
    }

    public ThemeManager getThemeManager()
    {
        return themeManager;
    }

    public void setThemeManager(ThemeManager themeManager)
    {
        this.themeManager = themeManager;
    }

    @HtmlSafe
    public String getSpaceTitles()
    {
        return GeneralUtil.htmlEncode(spaceTitles);
    }

    public PaginationSupport getPaginationSupport()
	{
		return paginationSupport;
	}

    public List<SearchResult> getSortedSubList()
    {
        return sortedSubList;
    }

    public int getSize()
    {
        return searchResults.size();
    }

    public String getQueryString()
    {
        return queryString;
    }

    public void setServletRequest(HttpServletRequest httpServletRequest)
    {
        this.request = httpServletRequest;
    }

    public ConfluenceActionSupport getConfluenceActionSupport()
    {
        if (null == confluenceActionSupport)
            confluenceActionSupport = GeneralUtil.newWiredConfluenceActionSupport();
        return confluenceActionSupport;
    }

    public void setSearchManager(SearchManager searchManager)
    {
        this.searchManager = searchManager;
    }

    public void setSpaceManager(SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public void setSearchMapper(LuceneSearchMapper searchMapper)
    {
        this.searchMapper = searchMapper;
    }

    public void setLuceneSearcher(LuceneSearcher searcher)
    {
        this.searcher=searcher;
    }

    /**
     * Should be autowired by Spring.
     */
    public void setHttpContext(HttpContext httpContext)
    {
        this.httpContext = httpContext;
    }

    //methods for SpaceAware
    public boolean isSpaceRequired()
    {
        return false;
    }

    public boolean isViewPermissionRequired()
    {
        return false;
    }

    public void setStartIndex(int startIndex)
    {
        this.startIndex = startIndex;
    }

    public void setQueryString(String queryString)
    {
        this.queryString = queryString;
    }

    public void setType(String[] types)
    {
        this.types = types;
    }

    public void setWhere(String where)
    {
        this.where = where;
    }

    public String execute() throws Exception
    {

        //********************
        //Get Parameters from URL
        //********************
        request = httpContext.getRequest();
//        queryString = request.getParameter("queryString");
//        if (request.getParameter("startIndex") != null)
//           startIndex = Integer.parseInt(request.getParameter("startIndex"));

        //********************
        //Get Parameters from Config
        //********************
        String spaceKey = where;
        if (spaceKey == null)
           return ERROR;

        space = spaceManager.getSpace(spaceKey);
        String[] spaceParamArray = spaces;
        StringBuilder spaceTitleStringBuilder = new StringBuilder();
        if (spaceParamArray == null)
        {
            spaceKeySet.add(space.getKey()); //default
            spaceTitles = spaceManager.getSpace(spaceKey).getName();
        }
        else if (spaceParamArray.length==1)
        {
            spaceKeySet.add(spaceParamArray[0]);
            spaceTitles = spaceManager.getSpace(spaceKey).getName();
        }
        else if (spaceParamArray.length==2)
        {
            for (int i=0;i<spaceParamArray.length;i++)
            {
                spaceKeySet.add(spaceParamArray[i]);
                spaceTitleStringBuilder.append(spaceManager.getSpace(spaceParamArray[i]).getName());
                if (i==0)
                  spaceTitleStringBuilder.append(" and ");
            }
            spaceTitles = spaceTitleStringBuilder.toString();
        }
        else
        {
            for (int i=0;i<spaceParamArray.length;i++)
            {
                spaceKeySet.add(spaceParamArray[i]);
                spaceTitleStringBuilder.append(spaceManager.getSpace(spaceParamArray[i]).getName());
                if (i+2<spaceParamArray.length)
                  spaceTitleStringBuilder.append(", ");
                else if (i+1<spaceParamArray.length)
                  spaceTitleStringBuilder.append(" and ");
            }
            spaceTitles = spaceTitleStringBuilder.toString();
         }

        //just pages and blogs - its more targeted that way
        //   types = request.getParameterValues("type");//surveyFeedbackDao.getSurveyConfig(space.getDescription()).getAttachmentTypes().split(",");//"page,blog,attachment".split(",");

        //********************
        //Perform Search
        //********************

        performSearch();

        //******************
        //Pagination Support
        //******************
        paginationSupport = new PaginationSupport(MAX_RESULTS_PER_PAGE);
        paginationSupport.setStartIndex(startIndex);
        paginationSupport.setTotal(searchResults.getAll().size());

        //******************
        //set searchResults for pagination
        //******************
        sortedSubList = getRankedResults(searchResults).getPage(startIndex,MAX_RESULTS_PER_PAGE);

       return SUCCESS;
    }

    public ListBuilder<SearchResult> getRankedResults(final SearchResults fullList)
    {
        return DefaultListBuilder.newInstance(new ListBuilderCallback<SearchResult>()
        {
            public List<SearchResult> getElements(int offset, int maxResults)
            {
                if (offset + maxResults < getAvailableSize())
                   return fullList.getAll().subList(offset,offset+maxResults);
                else
                   return fullList.getAll().subList(offset,getAvailableSize());
            }

            public int getAvailableSize()
            {
                return fullList.getAll().size();
            }
        });
    }

    private Query siteSearchQuery(SearchQueryParameters searchQueryParams)
	{
		Set<SearchQuery> scopedQuery = new HashSet<SearchQuery>();

		if (StringUtils.isBlank(searchQueryParams.getQuery()))
			scopedQuery.add(AllQuery.getInstance());
		else
            scopedQuery.add(new MultiTextFieldQuery(searchQueryParams.getQuery(), "title", "labelText", "contentBody", "filename", "username", "fullName", "email", "from", "recipients"));

        if (searchQueryParams.getContentTypes() != null && !searchQueryParams.getContentTypes().isEmpty())
            scopedQuery.add(new ContentTypeQuery(searchQueryParams.getContentTypes()));

        if (searchQueryParams.getLastModified() != null)
			scopedQuery.add(new DateRangeQuery(searchQueryParams.getLastModified(), DateRangeQuery.DateRangeQueryType.MODIFIED));

		if (searchQueryParams.getCategory() != null)
			scopedQuery.add(new SpaceCategoryQuery(searchQueryParams.getCategory()));

		if (searchQueryParams.getSpaceKeys() != null && !searchQueryParams.getSpaceKeys().isEmpty())
			scopedQuery.add(new InSpaceQuery(searchQueryParams.getSpaceKeys()));

        if (searchQueryParams.getAttachmentTypes() != null && !searchQueryParams.getAttachmentTypes().isEmpty())
			scopedQuery.add(new AttachmentTypeQuery(searchQueryParams.getAttachmentTypes()));

        ConfluenceUser contributor = searchQueryParams.getContributor();
		if (contributor != null && StringUtils.isNotBlank(contributor.getName()))
			scopedQuery.add(contributorQuery(searchQueryParams.getContributor().getName()));

		BooleanQuery query = new BooleanQuery(scopedQuery, null, null);

        return new BoostingQuery(searchMapper.convertToLuceneQuery(query), searchQueryParams, new KbCompositeScoreBoostingStrategy(surveySettingsManager));
	}

	private SearchQuery contributorQuery(String contributor)
	{
		Set<SearchQuery> subQueries = new HashSet<SearchQuery>();

		subQueries.add(new CreatorQuery(contributor)); // give a boost to results that this contributor has created
		subQueries.add(new ContributorQuery(contributor));

		return BooleanQuery.composeOrQuery(subQueries);
	}

    private void performSearch() throws InvalidSearchException
    {
        SearchQueryParameters searchQueryParams = new SearchQueryParameters(queryString);
        if (types != null)
        {
            Set<ContentTypeEnum> resultTypes = new HashSet<ContentTypeEnum>();
            StringBuilder sb = new StringBuilder();
            for (int it = 0; it < types.length; it++)
            {
                if (types[it] != null)
                {
                    if (types[it].equals("page"))
                    {
                        resultTypes.add(ContentTypeEnum.PAGE);
                    }
                    else if (types[it].equals("blogpost"))
                    {
                        resultTypes.add(ContentTypeEnum.BLOG);
                    }
                    else if (types[it].equals("mail"))
                    {
                        resultTypes.add(ContentTypeEnum.MAIL);
                    }
                    else if (types[it].equals("attachment"))
                    {
                        resultTypes.add(ContentTypeEnum.ATTACHMENT);
                    }
                    else if (types[it].equals("comment"))
                    {
                        resultTypes.add(ContentTypeEnum.COMMENT);
                    }
                    else
                    {
                        getConfluenceActionSupport().addActionError(getConfluenceActionSupport().getText("search.error.unknown-content-type"));
                    }

                    sb.append("type=");
                    sb.append(types[it]);
                    sb.append("&");
                }
                else
                {
                    getConfluenceActionSupport().addActionError(getConfluenceActionSupport().getText("search.error.unknown-content-type"));
                }
            }
            searchQueryParams.setContentTypes(resultTypes);
            typeString = sb.toString();
        }
        else
        {
            typeString="type="+ContentTypeEnum.PAGE+"&";
        }
        searchQueryParams.setSpaceKeys(spaceKeySet);
        Query query = siteSearchQuery(searchQueryParams);
        try
		{
	        Sort sort = null;
	        LuceneResultFilter resultFilter = searchMapper.convertToLuceneResultFilter( new SubsetResultFilter(0, OVERALL_MAX));
			Filter searchFilter = searchMapper.convertToLuceneSearchFilter(SiteSearchPermissionsSearchFilter.getInstance());

            long now = 0;
			if (log.isDebugEnabled())
                now = System.currentTimeMillis();

             searchResults = searcher.search(query, sort, searchFilter, resultFilter);

            if (log.isDebugEnabled())
                log.debug("Query time = " + (System.currentTimeMillis() - now) + "ms, Query = " + query);

    	}
		catch (IllegalArgumentException e)
		{
			throw new IllegalArgumentException("Invalid search query found in specified search.", e);
		}
		catch (LuceneMapperNotFoundException e)
		{
			throw new InvalidSearchException("A lucene mapper could not be found to map the specified search.", e);
		}
    }
}
