package com.atlassian.confluence.plugins.knowledgebase.results;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyConfig;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAdminAction;
import com.atlassian.sal.api.websudo.WebSudoRequired;

@WebSudoRequired
public class KbRollupAction extends AbstractSpaceAdminAction
{
    private PageManager pageManager;
    private String baseUrl;
    private SurveyConfig surveyConfig;
    private SurveyFeedbackManager surveyFeedbackManager;

    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }

    public String execute() throws Exception
    {
        space = this.getSpace();
        surveyConfig = surveyFeedbackManager.getSurveyConfig(space.getDescription());
        baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
        return SUCCESS;
    }

    public SurveyConfig getSurveyConfig()
    {
        return surveyConfig;
    }

    public void setPageManager (PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public PageManager getPageManager()
    {
        return pageManager;
    }

    public String getBaseUrl()
    {
        return baseUrl;
    }

    public String getSpaceBallotCount(Space space)
    {
        return surveyFeedbackManager.getSpaceBallotCount(space);
    }

    public String getSpaceYesNoTotals(String questionId)
    {
        return surveyFeedbackManager.getSpacePrimaryYesNoTotal(questionId);
    }

}
