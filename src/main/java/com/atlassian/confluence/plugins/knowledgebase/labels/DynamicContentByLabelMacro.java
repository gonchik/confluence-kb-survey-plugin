package com.atlassian.confluence.plugins.knowledgebase.labels;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;



public class DynamicContentByLabelMacro extends BaseMacro
{
    private static final Logger log = Logger.getLogger(DynamicContentByLabelMacro.class);
    private static final String TEMPLATE_NAME = "com/atlassian/confluence/plugins/knowledgebase/labels/kblabel.vm";
    private static final int DEFAULT_NUMBER = 10;
    private ConfluenceActionSupport confluenceActionSupport;
    protected LabelManager labelManager;
    private List<Label> labelNames = new LinkedList<Label>();
    
    public LabelManager getLabelManager()
    {
        return labelManager;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    
    public String execute(Map params, String body, RenderContext renderContext) throws MacroException
    {
        PageContext pageContext;
        if (renderContext instanceof PageContext)
            pageContext = (PageContext) renderContext;
        else
            throw new MacroException(getConfluenceActionSupport().getText("kbsearch.error.macro-works-in-only-page"));

        final Map<String, Object> contextMap = getMacroVelocityContext();

        Map<String, String> typeSafeParams = (Map<String, String>) params;
        //Get space parameter
        String spaceParam;
        if (typeSafeParams.containsKey("spaces"))
            spaceParam = typeSafeParams.get("spaces");
        else
        {
            spaceParam = pageContext.getSpaceKey();
        }

        boolean colorParam = false; // colors only on request
        if (typeSafeParams.containsKey("colours"))
            colorParam = Boolean.parseBoolean(typeSafeParams.get("colours"));
        if (typeSafeParams.containsKey("colors"))
            colorParam = Boolean.parseBoolean(typeSafeParams.get("colors"));

        String maxResults = typeSafeParams.get("maxResults");
        Integer max = Integer.valueOf(DEFAULT_NUMBER);
        try
        {
            if (maxResults != null)
               max = Integer.parseInt(maxResults);
        }
        catch (NumberFormatException e)
        {
            return "Error parsing number parameter in kblabel macro.";
        }

        boolean showSpace = true;
        if (typeSafeParams.containsKey("showSpace"))
            showSpace = typeSafeParams.get("showSpace").equals("true");

        boolean showLabels = true;
        if (typeSafeParams.containsKey("showLabels"))
            showLabels = typeSafeParams.get("showLabels").equals("true");

        List<Label> nonPersonalLabelNames = new LinkedList<Label>();
        labelNames = pageContext.getEntity().getLabels();
        for (Label l : labelNames)
        {
            if (l!=null&&!l.getType().equals("label:my")) //remove favourites and personal labels
                 nonPersonalLabelNames.add(l);
        }

        contextMap.put("maxResults", max);
        contextMap.put("colors",colorParam);
        contextMap.put("spaces",spaceParam);
        contextMap.put("labels",nonPersonalLabelNames);
        contextMap.put("helper",getConfluenceActionSupport().getHelper());
        contextMap.put("showSpace",showSpace);
        contextMap.put("showLabels",showLabels);

        return VelocityUtils.getRenderedTemplate(TEMPLATE_NAME, contextMap);

    }

/// CLOVER:OFF
    @SuppressWarnings("unchecked")
    protected Map<String, Object> getMacroVelocityContext()
    {
        // current MacroUtils returns untyped map, but its keys are always
        // strings and we don't want to restrict what it can hold
        return MacroUtils.defaultVelocityContext();
    }
///CLOVER:ON

    protected ConfluenceActionSupport getConfluenceActionSupport()
    {
        if (null == confluenceActionSupport) confluenceActionSupport = GeneralUtil.newWiredConfluenceActionSupport();
        return confluenceActionSupport;
    }
}
