package com.atlassian.confluence.plugins.knowledgebase.config;


public class SurveyQuestion
{
    private boolean active;
    private String question;
    private String id;

    public SurveyQuestion(String questionText, String Id)
    {
        question = questionText;
        active = true;
        id = Id;
    }

    public void setActive(boolean act)
    {
        active = act;
    }

    public void setQuestion(String q)
    {
        question = q;
    }

    public boolean isActive()
    {
        return active;
    }

    public String getQuestion()
    {
        return question;
    }

    public void setId(String Id)
    {
        id = Id;
    }

    public String getId()
    {
        return id;
    }

}
