package com.atlassian.confluence.plugins.knowledgebase;

import static com.atlassian.confluence.plugins.knowledgebase.Constants.CONFIG;
import static com.atlassian.confluence.plugins.knowledgebase.Constants.GA_NULL_VALUE;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyConfig;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyQuestion;
import com.atlassian.confluence.plugins.knowledgebase.results.SurveyGaResult;
import com.atlassian.confluence.plugins.knowledgebase.results.SurveyResult;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.thoughtworks.xstream.XStream;


import com.atlassian.activeobjects.external.*;

import java.util.Date;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;

import net.java.ao.DBParam;
import net.java.ao.Query;

public class DefaultSurveyFeedbackManager implements SurveyFeedbackManager {
	private ActiveObjects ao;
	private SurveyFeedbackDao surveyFeedbackDao;
	private ContentPropertyManager contentPropertyManager;
	private SurveySettingsManager surveySettingsManager;
	private final XStream xstream = new XStream();
	

	public DefaultSurveyFeedbackManager() {
		xstream.setClassLoader(getClass().getClassLoader());
		xstream.alias("surveyconfig", SurveyConfig.class);
		xstream.alias("surveyquestion", SurveyQuestion.class);
	}

	public void setActiveObjects(final ActiveObjects ao) {
		this.ao = ao;
	}

	public void setSurveyFeedbackDao(final SurveyFeedbackDao surveyFeedbackDao) {
		this.surveyFeedbackDao = surveyFeedbackDao;
	}

	public void setContentPropertyManager(
			final ContentPropertyManager contentPropertyManager) {
		this.contentPropertyManager = contentPropertyManager;
	}

	public void setSurveySettingsManager(
			final SurveySettingsManager surveySettingsManager) {
		this.surveySettingsManager = surveySettingsManager;
	}

	public SurveyConfig getSurveyConfig(SpaceDescription spaceDesc) {
		String xml = contentPropertyManager.getTextProperty(spaceDesc, CONFIG);
		if (xml == null || xml.equals(""))
			return new SurveyConfig();
		else
			return (SurveyConfig) xstream.fromXML(xml);
	}

	public void setSurveyConfig(SpaceDescription spaceDesc,
			SurveyConfig surveyConfig) {
		contentPropertyManager.setTextProperty(spaceDesc, CONFIG,
				xstream.toXML(surveyConfig));
	}

	public String getCompositeScore(AbstractPage page) {
		int yes = ao.count(Response.class, Query.select().where("PAGE_ID = ? and QUESTION_ID = ? and ANSWER = ?", page.getId(), page.getSpaceKey() + "0", true)) * surveySettingsManager.getYesIncrement();
		int no = ao.count(Response.class, Query.select().where("PAGE_ID = ? and QUESTION_ID = ? and ANSWER = ?", page.getId(), page.getSpaceKey() + "0", false)) * surveySettingsManager.getNoDecrement();
		return yes + no == 0 ? "" : Integer.toString(yes + no);
	}
	
	/* Composite score for a GA var.
	 * gaTag is the field to check (UTM_MEDIUM, UTM_SOURCE or UTM_CONTENT)
	 * gaValue is the content of the field specified in gaTag
	 */
	public String getCompositeScore(String gaTag, String gaValue, String spaceKey) {
		int yes = ao.count(Response.class, Query.select().where(gaTag + " = ? and SPACE_KEY = ? and QUESTION_ID = ? and DATE is not NULL and ANSWER = ?", gaValue, spaceKey, spaceKey + "0", true)) * surveySettingsManager.getYesIncrement();
		int no = ao.count(Response.class, Query.select().where(gaTag + " = ? and SPACE_KEY = ? and QUESTION_ID = ? and DATE is not NULL ANSWER = ?", gaValue, spaceKey, spaceKey + "0", false)) * surveySettingsManager.getNoDecrement();
		return yes + no == 0 ? "" : Integer.toString(yes + no);
	}

	// Number of Surveys for a page
	public String getNumberOfSurveys(AbstractPage page, String spaceKey) {
		return Integer.toString(ao.count(Response.class, Query.select().where("PAGE_ID = ? and SPACE_KEY = ? and QUESTION_ID = ?", page.getId(), spaceKey, spaceKey + "0")));
	}
	
	/* Number of Surveys for a GA var.
	 * gaTag is the field to check (UTM_MEDIUM, UTM_SOURCE or UTM_CONTENT)
	 * gaValue is the content of the field specified in gaTag
	 */
	public String getNumberOfSurveys(String gaTag, String gaValue, String spaceKey) {
		return Integer.toString(ao.count(Response.class, Query.select().where(gaTag + " = ? and SPACE_KEY = ? and DATE is not NULL", gaValue, spaceKey)));
	}

	public String getYesNoPercentage(AbstractPage page, String questionId) {
		int yes = ao.count(Response.class, Query.select().where("PAGE_ID = ? and QUESTION_ID = ? and ANSWER = ?", page.getId(), questionId, true));
		int no = ao.count(Response.class, Query.select().where("PAGE_ID = ? and QUESTION_ID = ? and ANSWER = ?", page.getId(), questionId, false));

		return yes == 0 ? NumberFormat.getPercentInstance().format(0) + " ("
				+ "0/" + no + ")" : no == 0 ? NumberFormat.getPercentInstance()
				.format(1) + " (" + yes + "/" + yes + ")" : NumberFormat
				.getPercentInstance().format(
						(double) yes / ((double) yes + (double) no))
				+ " (" + yes + "/" + (yes + no) + ")";

	}
	
	/* Calculate yes/no info for a GA var.
	 * gaTag is the field to check (UTM_MEDIUM, UTM_SOURCE or UTM_CONTENT)
	 * gaValue is the content of the field specified in gaTag
	 */
	public String getYesNoPercentage(String gaTag, String gaValue, String spaceKey, String questionId) {
		int yes, no;
		if (gaValue.equals(GA_NULL_VALUE)) {
			yes = ao.count(Response.class, Query.select().where(gaTag + " is NULL and SPACE_KEY = ? and QUESTION_ID = ? and DATE is not NULL and ANSWER = ?", spaceKey, questionId, true));
			no = ao.count(Response.class, Query.select().where(gaTag + " is NULL and SPACE_KEY = ? and QUESTION_ID = ? and DATE is not NULL and ANSWER = ?", spaceKey, questionId, false));
		} else {
			yes = ao.count(Response.class, Query.select().where(gaTag + " = ? and SPACE_KEY = ? and QUESTION_ID = ? and DATE is not NULL and ANSWER = ?", gaValue, spaceKey, questionId, true));
			no = ao.count(Response.class, Query.select().where(gaTag + " = ? and SPACE_KEY = ? and QUESTION_ID = ? and DATE is not NULL and ANSWER = ?", gaValue, spaceKey, questionId, false));
		}

		return yes == 0 ? NumberFormat.getPercentInstance().format(0) + " ("
				+ "0/" + no + ")" : no == 0 ? NumberFormat.getPercentInstance()
				.format(1) + " (" + yes + "/" + yes + ")" : NumberFormat
				.getPercentInstance().format(
						(double) yes / ((double) yes + (double) no))
				+ " (" + yes + "/" + (yes + no) + ")";

	}

	public String getSpaceBallotCount(Space space) {
		return String.valueOf(surveyFeedbackDao.getSpaceBallotCount(space));
	}

	public String getSpacePrimaryYesNoTotal(String questionId) {
		return surveyFeedbackDao.getSpacePrimaryYesNoTotal(questionId);
	}
	// Store a new answer setting the date to the current time and date
	public void addSurveyResult(AbstractPage page, String questionId,
			Boolean answer, String spaceKey, String utmSource, String utmMedium, String utmContent) {

		// AO store new answer
		ao.create(Response.class, new DBParam("QUESTION_ID", questionId), new DBParam("SPACE_KEY", spaceKey), new DBParam("ANSWER", answer),
				new DBParam("PAGE_ID", page.getId()), new DBParam("UTM_SOURCE", utmSource), new DBParam("UTM_MEDIUM", utmMedium), new DBParam("UTM_CONTENT", utmContent), new DBParam("DATE", new Date()));
	}
	// Store a new answer specifying a date
	public void addSurveyResult(AbstractPage page, String questionId,
			Boolean answer, String spaceKey, String utmSource, String utmMedium, String utmContent, Date date) {

		// AO store new answer
		ao.create(Response.class, new DBParam("QUESTION_ID", questionId), new DBParam("SPACE_KEY", spaceKey), new DBParam("ANSWER", answer),
				new DBParam("PAGE_ID", page.getId()), new DBParam("UTM_SOURCE", utmSource), new DBParam("UTM_MEDIUM", utmMedium), new DBParam("UTM_CONTENT", utmContent), new DBParam("DATE", date));
	}

	public List<String> getPageIdsSortedByCompositeScore(String spaceKey,
			Integer max) {
		return surveyFeedbackDao
				.getPageIdsSortedByCompositeScore(spaceKey, max, surveySettingsManager.getYesIncrement(), surveySettingsManager.getNoDecrement());
	}

	public List<Long> getUnsurveyedPageIdList(Space space, PageManager pageManager) {
		return surveyFeedbackDao.getUnsurveyedPages(space, pageManager);
	}

	public int getNumberOfSurveyedPages(String spaceKey) {
		return surveyFeedbackDao.getNumberOfSurveyedPages(spaceKey);
	}
	
	public int getNumberOfGaValues(String gaTag, String spaceKey) {
		/* Buggy AO don't let me do
		 * 		return ao.find(Response.class, gaTag, Query.select(gaTag).distinct().where("SPACE_KEY = ?", spaceKey)).length;
		 * nor
		 * 		return ao.count(Response.class, Query.select(gaTag).distinct().where("SPACE_KEY = ?", spaceKey));
		 * 
		 * Check:
		 * 		https://ecosystem.atlassian.net/browse/AO-398
		 * 		https://ecosystem.atlassian.net/browse/AO-399
		 * 
		 * For more information.
		 * 
		 */
		List<String> distinctFields= new LinkedList<String>();
		Response [] responses = ao.find(Response.class, Query.select().distinct().where("SPACE_KEY = ? and DATE is not NULL", spaceKey));
    	for (int i = 0; i < responses.length; i++) {
    		String currentFieldValue = "";
    		if (gaTag.equals("UTM_SOURCE"))
    			currentFieldValue = responses[i].getUtmSource();
    		if (gaTag.equals("UTM_MEDIUM"))
    			currentFieldValue = responses[i].getUtmMedium();
    		if (gaTag.equals("UTM_CONTENT"))
    			currentFieldValue = responses[i].getUtmContent();
    		if (!distinctFields.contains(currentFieldValue))
    			distinctFields.add(currentFieldValue);
    	}
    	return distinctFields.size();
	}

	public List<SurveyResult> getPagedResults(String order, String spaceKey,
			int offset, int max) {
		return surveyFeedbackDao.getPagedResults(order, spaceKey, offset, max, surveySettingsManager.getYesIncrement(), surveySettingsManager.getNoDecrement());
	}
	
	public List<SurveyGaResult> getPagedResults(String gaTag, String order, String spaceKey, int offset, int max) {
		return surveyFeedbackDao.getPagedResults(gaTag, order, spaceKey, offset, max, surveySettingsManager.getYesIncrement(), surveySettingsManager.getNoDecrement());
	}

	public void evictSurveyResults(AbstractPage page) {
		Response[] rowsToDelete = ao.find(Response.class, Query.select().where("PAGE_ID = ?", page.getId()));
		for (int i = 0; i < rowsToDelete.length; i++)
			ao.delete(rowsToDelete[i]);
	}

	public boolean hasPageResult(AbstractPage page, String spaceKey) {
		return (ao.find(Response.class, Query.select().where("PAGE_ID = ?", page.getId())).length > 0);
	}
	
	public boolean hasGaResult(String gaTag, String spaceKey) {
		return (ao.find(Response.class, Query.select(gaTag).distinct().where("SPACE_KEY = ?", spaceKey)).length > 0);
	}

}
