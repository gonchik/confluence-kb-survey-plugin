package com.atlassian.confluence.plugins.knowledgebase.config;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;

import javax.servlet.http.HttpSession;
import java.util.*;

public class SurveyConfig
{
    private String response;
    private String responseUser;
    private Map<String, SurveyQuestion> questionMap;
    private String primaryQuestionId;
    private String primaryQuestion;

    public SurveyConfig()
    {
        questionMap = new HashMap<String,SurveyQuestion>();
    }

    public void setPrimaryQuestionId(String primaryQuestionId)
    {
        this.primaryQuestionId = primaryQuestionId;
    }

    public String getPrimaryQuestionId()
    {
        return primaryQuestionId;
    }

    public void setPrimaryQuestion(String primaryQuestion)
    {
        this.primaryQuestion = primaryQuestion;
    }

    public String getPrimaryQuestion()
    {
        return primaryQuestion;
    }

    public void setResponse(String r)
    {
        response = r;
    }

    public String getResponse(HttpSession session)
    {
       if (AuthenticatedUserThreadLocal.getUser() != null)
       {
           return getResponseUser();
       }
        else
           return getResponse();
    }

    @HtmlSafe
    public String getResponse()
    {
        return response;
    }


    public void setResponseUser(String r)
    {
        responseUser = r;
    }

    public String getResponseUser()
    {
        return responseUser;
    }

    public Map<String,SurveyQuestion> getQuestionMap()
    {
        return questionMap;
    }

    public void addQuestion(SurveyQuestion q)
    {
        questionMap.put(q.getId(),q);
    }

    public void deactivateQuestion(String questionId)
    {
        questionMap.get(questionId).setActive(false);
    }

    public void activateQuestion(String questionId)
    {
        questionMap.get(questionId).setActive(true);
    }

    public List<SurveyQuestion> getActiveQuestionList()
    {
        LinkedList<SurveyQuestion> activeQuestions = new LinkedList<SurveyQuestion>();
        for (SurveyQuestion q : questionMap.values())
        {
            if (q.isActive())
                activeQuestions.add(q);
        }

        return activeQuestions;
    }

}
