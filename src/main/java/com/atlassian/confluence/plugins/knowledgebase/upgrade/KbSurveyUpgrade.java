package com.atlassian.confluence.plugins.knowledgebase.upgrade;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.bandana.BandanaContext;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ConfluencePropertySetManager;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.plugins.knowledgebase.SurveySettingsManager;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyQuestion;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.thoughtworks.xstream.XStream;

import static com.atlassian.confluence.plugins.knowledgebase.Constants.BUILD_NUMBER;
import static com.atlassian.confluence.plugins.knowledgebase.Constants.PLUGIN_KEY;
import static com.atlassian.confluence.plugins.knowledgebase.Constants.UPGRADE_DESC;
import static com.atlassian.confluence.plugins.knowledgebase.Constants.RESULTS;
import static com.atlassian.confluence.plugins.knowledgebase.Constants.YES_INCREMENT_KEY;
import static com.atlassian.confluence.plugins.knowledgebase.Constants.NO_DECREMENT_KEY;
import static com.atlassian.confluence.plugins.knowledgebase.Constants.BOOST_AMPLIFIER_KEY;
import static com.atlassian.confluence.plugins.knowledgebase.Constants.BOOST_MAXIMUM_KEY;
import static com.atlassian.confluence.plugins.knowledgebase.Constants.BOOST_MINIMUM_KEY;

public class KbSurveyUpgrade implements PluginUpgradeTask {
	private BandanaManager bandanaManager;
	private SpaceManager spaceManager;
	private PageManager pageManager;
	private ConfluencePropertySetManager propertySetManager;
	private SurveySettingsManager surveySettingsManager;
	private SurveyFeedbackManager surveyFeedbackManager;
	
	public void setPropertySetManager(
			final ConfluencePropertySetManager propertySetManager) {
		this.propertySetManager = propertySetManager;
	}
	public void setSurveySettingsManager(
			final SurveySettingsManager surveySettingsManager) {
		this.surveySettingsManager = surveySettingsManager;
	}
	
	public void setBandanaManager(
			final BandanaManager bandanaManager) {
		this.bandanaManager = bandanaManager;
	}

    public void setSpaceManager (SpaceManager spaceManager)
    {
        this.spaceManager = spaceManager;
    }
    public void setPageManager (PageManager pageManager)
    {
        this.pageManager = pageManager;
    }
    public void setSurveyFeedbackManager(SurveyFeedbackManager surveyFeedbackManager)
    {
        this.surveyFeedbackManager = surveyFeedbackManager;
    }
	
	public Collection<Message> doUpgrade() throws Exception {
		// Generate a response in AO tables for all the existing responses
		// Get all spaces
		List<Space> spaceList = spaceManager.getAllSpaces();
		Iterator<Space> spaceIterator = spaceList.iterator();
		while (spaceIterator.hasNext()) {
			// Get list of questions for the space
			Space space = spaceIterator.next();
			List<SurveyQuestion> questionsList= surveyFeedbackManager.getSurveyConfig(space.getDescription()).getActiveQuestionList();
			if (questionsList.size() > 0) {
				// Survey plugin configured for this space
				// Get all pages in the space
				List<Page> pageList = pageManager.getPages(space, false);
				Iterator<Page> pageIterator = pageList.iterator();
				while (pageIterator.hasNext()) {
					// Go through the pages in the space
					Page page = pageIterator.next();
					Iterator<SurveyQuestion> questionIterator = questionsList.iterator();
					// Migrate primary question
					String questionId = space.getKey() + "0";
					// Generate results for yes answers
					int replyCount = propertySetManager.getPropertySet(page).getInt(
							RESULTS + questionId + ".yes");
					for (int i =0; i < replyCount; i++)
						surveyFeedbackManager.addSurveyResult(page, questionId, true, space.getKey(), null, null, null, null);
					// Generate results for no answers
					replyCount = propertySetManager.getPropertySet(page).getInt(RESULTS + questionId + ".no");
					for (int i =0; i < replyCount; i++)
						surveyFeedbackManager.addSurveyResult(page, questionId, false, space.getKey(), null, null, null, null);
					while (questionIterator.hasNext()) {
						// Go through the results for every question in the page
						questionId = questionIterator.next().getId();
						// Generate results for yes answers
						replyCount = propertySetManager.getPropertySet(page).getInt(
								RESULTS + questionId + ".yes");
						for (int i =0; i < replyCount; i++)
							surveyFeedbackManager.addSurveyResult(page, questionId, true, space.getKey(), null, null, null, null);
						// Generate results for no answers
						replyCount = propertySetManager.getPropertySet(page).getInt(RESULTS + questionId + ".no");
						for (int i =0; i < replyCount; i++)
							surveyFeedbackManager.addSurveyResult(page, questionId, false, space.getKey(), null, null, null, null);
					}
					
/*	The deletion of the properties once the content is migrated was discarded as suggested in the code review. In 
 * future versions the upgrade task should filter by build number to see if they need to be transformed (no build
 * number) or not (build number = 1).
 */					
				}
			}
		}
		// Update settings from Bandana
		ConfluenceBandanaContext context = new ConfluenceBandanaContext();
		String bandanaValue = (String) bandanaManager.getValue(context, YES_INCREMENT_KEY);
		if (bandanaValue != null) {
			surveySettingsManager.setYesIncrememt(Integer.parseInt(bandanaValue));
			bandanaManager.removeValue(context, YES_INCREMENT_KEY);
		}
		bandanaValue = (String) bandanaManager.getValue(context, NO_DECREMENT_KEY);
		if (bandanaValue != null) {
			surveySettingsManager.setNoDecrememt(Integer.parseInt(bandanaValue));
			bandanaManager.removeValue(context, NO_DECREMENT_KEY);
		}
		bandanaValue = (String) bandanaManager.getValue(context, BOOST_AMPLIFIER_KEY);
		if (bandanaValue != null) {
			surveySettingsManager.setBoostAmplifier(Integer.parseInt(bandanaValue));
			bandanaManager.removeValue(context, BOOST_AMPLIFIER_KEY);
		}
		bandanaValue = (String) bandanaManager.getValue(context, BOOST_MAXIMUM_KEY);
		if (bandanaValue != null) {
			surveySettingsManager.setBoostMaximum(Integer.parseInt(bandanaValue));
			bandanaManager.removeValue(context, BOOST_MAXIMUM_KEY);
		}
		bandanaValue = (String) bandanaManager.getValue(context, BOOST_MINIMUM_KEY);
		if (bandanaValue != null) {
			surveySettingsManager.setBoostMinimum(Integer.parseInt(bandanaValue));
			bandanaManager.removeValue(context, BOOST_MINIMUM_KEY);
		}
		
		return null;
	}

	public int getBuildNumber() {
		return BUILD_NUMBER;
	}

	public String getPluginKey() {
		return PLUGIN_KEY;
	}

	public String getShortDescription() {
		return UPGRADE_DESC;
	}

}
