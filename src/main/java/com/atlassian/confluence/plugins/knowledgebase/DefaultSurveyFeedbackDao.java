package com.atlassian.confluence.plugins.knowledgebase;

import com.atlassian.activeobjects.external.*;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.knowledgebase.results.SurveyGaResult;
import com.atlassian.confluence.plugins.knowledgebase.results.SurveyResult;
import com.atlassian.confluence.spaces.Space;
import static com.atlassian.confluence.plugins.knowledgebase.Constants.GA_NULL_VALUE;
import net.java.ao.Query;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class DefaultSurveyFeedbackDao implements SurveyFeedbackDao
{
	private ActiveObjects ao;

    public void setActiveObjects(final ActiveObjects ao)
    {
    	this.ao = ao;
    }
    
    // Get total surveys (primary question answered) per space
    public int getSpaceBallotCount(final Space space)
    {
    	// As QUESTION_ID contains the space key, it should not be needed to filter by SPACE_KEY but it is added in case this changes in the future.
    	return ao.count(Response.class, Query.select().where("SPACE_KEY = ? and QUESTION_ID = ?", space.getKey(), space.getKey() + "0"));
    }
    // As this method has the question ID as a parameter, it should be valid for any question, not only the primary
    // Note that question ID contains the space id
    public String getSpacePrimaryYesNoTotal(final String questionId)
    {
    	int no = 0, yes = 0;
    	yes = ao.count(Response.class, Query.select().where("QUESTION_ID = ? and ANSWER = ?", questionId, true));
    	no = ao.count(Response.class, Query.select().where("QUESTION_ID = ? and ANSWER = ?", questionId, false));
    	return (yes == 0 && no == 0) ? "" : NumberFormat.getPercentInstance().format((double) yes / (double) (no + yes)) + " (" + yes + "/" + (no + yes) + ")";
    }

    @SuppressWarnings("unchecked")
    public List<String> getPageIdsSortedByCompositeScore(final String spaceKey, final Integer max, Integer yesIncrement, Integer noDecrement)
    {
    	final List<String> pageIdList = new LinkedList<String>();
    	HashMap<Long, Integer> pagescore = new HashMap<Long, Integer>();
    	Response[] responses;
    	
    	// Get disticnt pageids
    	responses = ao.find(Response.class, Query.select("PAGE_ID").distinct().where("SPACE_KEY = ?", spaceKey));
    	for (int i = 0; i < responses.length; i++) {
    		int no = 0, yes = 0;
    		long pageid = responses[i].getPageId();
    		// Calculate composite score for that page
    		yes = ao.count(Response.class, Query.select().where("PAGE_ID = ? and QUESTION_ID = ? and ANSWER = ?", pageid, spaceKey + "0", true)) * yesIncrement;
    		no = ao.count(Response.class, Query.select().where("PAGE_ID = ? and QUESTION_ID = ? and ANSWER = ?", pageid, spaceKey + "0", false)) * noDecrement;
    		// Store pageid and composite score
    		pagescore.put(pageid, yes + no);
    		// Add the pageid in the correspondent position in the sorted list
    		if (i == 0 ||  pagescore.get(Long.parseLong(pageIdList.get(pageIdList.size() - 1))) >= pagescore.get(pageid))
    			// First or last item in the list
    			pageIdList.add(String.valueOf(pageid));
    		else {
    			// Insert pageid in the correct position 
    			for (int j = 0; j < pageIdList.size(); j++) {
    				if ((pagescore.get(Long.parseLong(pageIdList.get(j))) <= pagescore.get(pageid))) {
    					pageIdList.add(j, String.valueOf(pageid));
    					break;
    				}
    			}
    				
    		}
    	}
    	
    	return (pageIdList.size() > max)? pageIdList.subList(0, max): pageIdList;
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUnsurveyedPages(final Space space, final PageManager pageManager)
    {
    	List<Page> allPages = pageManager.getPages(space, true);
    	List<Long> unsurveyedPages = new LinkedList<Long>();
    	List<Long> surveyedPages = new LinkedList<Long>();
    	Response[] surveyedResponses = ao.find(Response.class, Query.select("PAGE_ID").distinct().where("SPACE_KEY = ?", space.getKey()));
    	for (int i = 0; i < surveyedResponses.length; i++)
    		surveyedPages.add(surveyedResponses[i].getPageId());
    	for (int i = 0; i < allPages.size(); i++) {
    		if (!surveyedPages.contains(allPages.get(i).getId()))
    			unsurveyedPages.add(allPages.get(i).getId());
    	}
    	
    	
    	return unsurveyedPages;
    }


    public int getNumberOfSurveyedPages(final String spaceKey)
    {
    	return ao.find(Response.class, Query.select("PAGE_ID").distinct().where("SPACE_KEY = ?", spaceKey)).length;
    }

    @SuppressWarnings("unchecked")
    public List<SurveyResult> getPagedResults(final String order, final String spaceKey, final int offset, final int max, Integer yesIncrement, Integer noDecrement)
    {
    	List<SurveyResult> pageList = new LinkedList<SurveyResult>();
    	Response[] responses;
    	HashMap<Long, Integer> pagescore = new HashMap<Long, Integer>();
    	HashMap<Long, Integer> pagesTotalSurveys = new HashMap<Long, Integer>();
    	boolean asc = "composite_asc".equals(order) || "total_asc".equals(order);
    	boolean total = "total_asc".equals(order) || "total_desc".equals(order);
    	
    	// Get disticnt pageids
    	responses = ao.find(Response.class, Query.select("PAGE_ID").distinct().where("SPACE_KEY = ?", spaceKey));
    	
    	for (int i = 0; i < responses.length; i++) {
    		SurveyResult surveyResult = new SurveyResult();
    		int no = 0, yes = 0;
    		long pageid = responses[i].getPageId();
    		surveyResult.setPageId(String.valueOf(pageid));
    		// Calculate composite score for that page
    		yes = ao.count(Response.class, Query.select().where("PAGE_ID = ? and QUESTION_ID = ? and ANSWER = ?", pageid, spaceKey + "0", true));
    		no = ao.count(Response.class, Query.select().where("PAGE_ID = ? and QUESTION_ID = ? and ANSWER = ?", pageid, spaceKey + "0", false));
    		// Store pageid and composite score
    		pagescore.put(pageid, yes  * yesIncrement + no * noDecrement);
    		surveyResult.setCompositeScore(String.valueOf(yes * yesIncrement + no * noDecrement));
    		// Get and store total surveys for that page
    		pagesTotalSurveys.put(pageid, yes + no);
    		surveyResult.setTotalSurveys(String.valueOf(yes + no));
    		if (total) {
    			// Sort by total surveys
    			// Add the pageid in the correspondent position in the sorted list
    			if (i == 0 ||  (pagesTotalSurveys.get(Long.valueOf(pageList.get(pageList.size() - 1).getPageId())) >= pagesTotalSurveys.get(pageid)))
    				// First or last item in the list
    				pageList.add(surveyResult);
    			else {
    				// Insert pageid in the right position 
    				for (int j = 0; j < pageList.size(); j++) {
    					if (Integer.valueOf(pageList.get(j).getTotalSurveys()) <= pagesTotalSurveys.get(pageid)) {
    						pageList.add(j, surveyResult);
    						break;
    					}
    				}
    			}
    		} else {
    			// Sort by composite score
    			// Add the pageid in the correspondent position in the sorted list
    			if (i == 0 ||  (pagescore.get(Long.valueOf(pageList.get(pageList.size() - 1).getPageId())) >= pagescore.get(pageid)))
    				// First or last item in the list
    				pageList.add(surveyResult);
    			else {
    				// Insert pageid in the correct position 
    				for (int j = 0; j < pageList.size(); j++) {
    					if (Integer.valueOf(pageList.get(j).getCompositeScore()) <= pagescore.get(pageid)) {
    						pageList.add(j, surveyResult);
    						break;
    					}
    				}
    			}
    		}
    		
      	}
    	if (asc) {
    		//Ascendant order, revert list
    		List<SurveyResult> reversedPageList = new LinkedList<SurveyResult>();
    		for (int i = pageList.size() -1; i >= 0; i--)
    			reversedPageList.add(pageList.get(i));
    		return reversedPageList.subList(offset, (offset + max < reversedPageList.size())? offset + max : reversedPageList.size());	
    	}
    	return pageList.subList(offset, (offset + max < pageList.size())? offset + max : pageList.size());
    }
    
    @SuppressWarnings("unchecked")
    public List<SurveyGaResult> getPagedResults(final String gaTag, final String order, final String spaceKey, final int offset, final int max, Integer yesIncrement, Integer noDecrement)
    {
    	List<SurveyGaResult> gaResultList = new LinkedList<SurveyGaResult>();
    	Response[] responses;
    	List<String> distinctFields= new LinkedList<String>();
    	HashMap<String, Integer> pagescore = new HashMap<String, Integer>();
    	HashMap<String, Integer> pagesTotalSurveys = new HashMap<String, Integer>();
    	boolean asc = "composite_asc".equals(order) || "total_asc".equals(order);
    	boolean total = "total_asc".equals(order) || "total_desc".equals(order);
    	
    	// Get distinct pageids
    	/* Note by Alex:
    	 * This would be the most straight way to get a list of all distinct fields, but because what I believe
    	 * a bug in Active Objects implementation, this line fails to execute successfully hence I had to find a workaround for it
    	 *   	responses = ao.find(Response.class, gaTag, Query.select(gaTag).distinct().where("SPACE_KEY = ? and DATE is not NULL", spaceKey)); */
    	responses = ao.find(Response.class, Query.select().distinct().where("SPACE_KEY = ? and DATE is not NULL", spaceKey));
    	for (int i = 0; i < responses.length; i++) {
    		String currentFieldValue = "";
    		if (gaTag.equals("UTM_SOURCE"))
    			currentFieldValue = responses[i].getUtmSource();
    		if (gaTag.equals("UTM_MEDIUM"))
    			currentFieldValue = responses[i].getUtmMedium();
    		if (gaTag.equals("UTM_CONTENT"))
    			currentFieldValue = responses[i].getUtmContent();
    		if (currentFieldValue == null)
    			currentFieldValue = GA_NULL_VALUE;
    		if (!distinctFields.contains(currentFieldValue))
    			distinctFields.add(currentFieldValue);
    	}
    	Iterator<String> it = distinctFields.iterator();
    	while (it.hasNext()) {
    		SurveyGaResult surveyResult = new SurveyGaResult();
    		int no = 0, yes = 0;
    		String gaValue = it.next();
    		surveyResult.setGaValue(gaValue);
    		surveyResult.setGaTag(gaTag);
    		
    		// Calculate composite score for that page
    		if (gaValue.equals(GA_NULL_VALUE)) {
    			yes = ao.count(Response.class, Query.select().where(gaTag + " is NULL and SPACE_KEY = ? and QUESTION_ID = ? and DATE is not NULL and ANSWER = ?", spaceKey, spaceKey + "0", true));
	    		no = ao.count(Response.class, Query.select().where(gaTag + " is NULL and SPACE_KEY = ? and QUESTION_ID = ? and DATE is not NULL and ANSWER = ?", spaceKey, spaceKey + "0", false));	
    		} else {
	    		yes = ao.count(Response.class, Query.select().where(gaTag + " = ? and SPACE_KEY = ? and QUESTION_ID = ? and DATE is not NULL and ANSWER = ?", gaValue, spaceKey, spaceKey + "0", true));
	    		no = ao.count(Response.class, Query.select().where(gaTag + " = ? and SPACE_KEY = ? and QUESTION_ID = ? and DATE is not NULL and ANSWER = ?", gaValue, spaceKey, spaceKey + "0", false));
    		}
    		// Store gaValue and composite score
    		pagescore.put(gaValue, yes  * yesIncrement + no * noDecrement);
    		surveyResult.setCompositeScore(String.valueOf(yes * yesIncrement + no * noDecrement));
    		// Get and store total surveys for that page
    		pagesTotalSurveys.put(gaValue, yes + no);
    		surveyResult.setTotalSurveys(String.valueOf(yes + no));
    		if (total) {
    			// Sort by total surveys
    			// Add the gaValue in the correspondent position in the sorted list
    			if (gaResultList.size() == 0 ||  (pagesTotalSurveys.get(gaResultList.get(gaResultList.size() - 1).getGaValue()) >= pagesTotalSurveys.get(gaValue)))
    				// First or last item in the list
    				gaResultList.add(surveyResult);
    			else {
    				// Insert gaValue in the correct position 
    				for (int j = 0; j < gaResultList.size(); j++) {
    					if (Integer.valueOf(gaResultList.get(j).getTotalSurveys()) <= pagesTotalSurveys.get(gaValue)) {
    						gaResultList.add(j, surveyResult);
    						break;
    					}
    				}
    			}
    		} else {
    			// Sort by composite score
    			// Add the gaValue in the correspondent position in the sorted list
    			if (gaResultList.size() == 0 ||  (pagescore.get(gaResultList.get(gaResultList.size() - 1).getGaValue()) >= pagescore.get(gaValue)))
    				// First or last item in the list
    				gaResultList.add(surveyResult);
    			else {
    				// Insert gaValue in the correct position 
    				for (int j = 0; j < gaResultList.size(); j++) {
    					if (Integer.valueOf(gaResultList.get(j).getCompositeScore()) <= pagescore.get(gaValue)) {
    						gaResultList.add(j, surveyResult);
    						break;
    					}
    				}
    			}
    		}
    		
      	}
    	if (asc) {
    		//Ascendant order, revert list
    		List<SurveyGaResult> reversedPageList = new LinkedList<SurveyGaResult>();
    		for (int i = gaResultList.size() -1; i >= 0; i--)
    			reversedPageList.add(gaResultList.get(i));
    		return reversedPageList.subList(offset, (offset + max < reversedPageList.size())? offset + max : reversedPageList.size());	
    	}
    	return gaResultList.subList(offset, (offset + max < gaResultList.size())? offset + max : gaResultList.size());
    }
}

