<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="Atlassian" url="${project.organization.url}" />
        <param name="configure.url">/admin/plugins/knowledgebase/configure.action</param>
    </plugin-info>
	<ao key="CKBSP-ao-module">
		<description>Configuration of the Active Objects service</description>
		<entity>com.atlassian.confluence.plugins.knowledgebase.Response</entity>
		<entity>com.atlassian.confluence.plugins.knowledgebase.Settings</entity>
	</ao>
   	<component-import key="ao" name="Active Objects components" interface="com.atlassian.activeobjects.external.ActiveObjects" />
    <resource type="i18n" name="i18n" location="com.atlassian.confluence.plugins.knowledgebase.KnowledgeBasePlugin" />
    <resource key="icons" name="icons/" type="download" location="com/atlassian/confluence/plugins/knowledgebase/images/icons"/>

    <macro name='kbsurvey' class="com.atlassian.confluence.plugins.knowledgebase.survey.KbSurveyMacro" key="kbsurvey"
            icon="/download/resources/confluence.plugins.knowledgebase.survey/icons/survey.jpg"
            documentation-url="https://labs.atlassian.com/wiki/display/KBSP/Home">
        <description>Knowledge Base Survey based on Space Configuration</description>
        <category name="reporting"/>
        <parameters/>
        <resource type="velocity" name="help" location="com/atlassian/confluence/plugins/knowledgebase/survey/surveyhelp.vm">
                  <param name="help-section" value="confluence"/>
        </resource>
    </macro>

    <macro name='dynamiccontentbylabel'
           class="com.atlassian.confluence.plugins.knowledgebase.labels.DynamicContentByLabelMacro"
           key="dynamiccontentbylabel"
           icon="/images/icons/macrobrowser/related-labels.png"
           documentation-url="https://labs.atlassian.com/wiki/display/KBSP/Home">
        <description>Lists pages that have labels in common with the current page.</description>
        <alias>kblabels</alias>
        <category name="confluence-content"/>
        <parameters>
            <parameter name="spaces" type="spacekey" multiple="true"/>
            <parameter name="maxResults" type="int"/>
            <parameter name="colors" type="boolean"/>
            <parameter name="showSpace" type="boolean"/>
            <parameter name="showLabels" type="boolean"/>
        </parameters>
        <resource type="velocity" name="help"
                  location="com/atlassian/confluence/plugins/knowledgebase/labels/kblabelhelp.vm">
            <param name="help-section" value="confluence"/>
        </resource>
    </macro>

    <macro name="kbtoparticles" class="com.atlassian.confluence.plugins.knowledgebase.results.TopArticlesMacro" key="kbtoparticles"
            icon="/download/resources/confluence.plugins.knowledgebase.survey/icons/top10.jpg"
            documentation-url="https://labs.atlassian.com/wiki/display/KBSP/Home">
        <description>Displays the top ranked Knowledge Base articles.</description>
        <category name="reporting"/>
        <resource type="velocity" name="help" location="com/atlassian/confluence/plugins/knowledgebase/results/kbtoparticleshelp.vm" >
            <param name="help-section" value="confluence"/>
        </resource>
        <parameters>
            <parameter name="space" type="spacekey" multiple="false"/>
            <parameter name="" type="int"/>
        </parameters>
    </macro>

   <macro name="kbsearch" class="com.atlassian.confluence.plugins.knowledgebase.search.KbSearchMacro" key="kbsearch"
           icon="/images/icons/macrobrowser/livesearch.png"
           documentation-url="https://labs.atlassian.com/wiki/display/KBSP/Home">
       <description>A QuickSearch Macro for use in conjunction with the Documentation Theme.</description>
       <category name="reporting"/>
       <parameters>
        <parameter name="spaces" type="spacekey" multiple="true"/>
        <parameter name="types" type="string" multiple="true"/>
       </parameters>
       <resource type="velocity" name="help" location="com/atlassian/confluence/plugins/knowledgebase/search/kbsearchhelp.vm">
            <param name="help-section" value="confluence"/>
        </resource>
    </macro>

    <web-resource name = "All other KB macro Resources" key='surveytaculous'>
        <resource type="download" name="KnowledgeBasePlugin.css" location="com/atlassian/confluence/plugins/knowledgebase/KnowledgeBasePlugin.css"/>
        <resource type="download" name="jquery.parsequery.min.js" location="com/atlassian/confluence/plugins/knowledgebase/jquery.parsequery.min.js"/>
        <resource type="download" name="jumpto.js" location="com/atlassian/confluence/plugins/knowledgebase/jumpto.js"/>
        <dependency>confluence.web.resources:ajs</dependency>
    </web-resource>

    <web-resource name = "KB Search Macro Resources" key='kb-search-macro'>
        <resource type="download" name="kbsearch.js" location="com/atlassian/confluence/plugins/knowledgebase/search/kbsearch.js" />
    </web-resource>

   <extractor name="Survey Extractor" key="surveyExtractor" class="com.atlassian.confluence.plugins.knowledgebase.search.KbSearchExtractor" priority="1000">
        <description>The Extractor used to learn what pages have the highest composite scores.</description>
    </extractor>
<!-- We'll add this manually instead of through the filter chain, so it only appplies to the KB search
    <lucene-boosting-strategy name="Composite Score Boosting Strategy" key="compositeScoreBoosting" class="com.atlassian.confluence.plugins.knowledgebase.search.KbCompositeScoreBoostingStrategy"/>
-->
    <component name="Survey Settings Manager" key="surveySettingsManager" alias="surveySettingsManager"
               class="com.atlassian.confluence.plugins.knowledgebase.DefaultSurveySettingsManager">
        <interface>com.atlassian.confluence.plugins.knowledgebase.SurveySettingsManager</interface>
    </component>
    
    <component name="Survey Feedback DAO" key="surveyFeedbackDao" alias="surveyFeedbackDao"
               class="com.atlassian.confluence.plugins.knowledgebase.DefaultSurveyFeedbackDao">
        <interface>com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackDao</interface>
    </component>

    <component name="Survey Feedback Manager" key="surveyFeedbackManager" alias="surveyFeedbackManager"
               class="com.atlassian.confluence.plugins.knowledgebase.DefaultSurveyFeedbackManager">
        <interface>com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager</interface>
    </component>
    
    <component name="KB Survey Upgrade Task" key="KbSurveyUpgrade" alias="KbSurveyUpgrade"
			   class="com.atlassian.confluence.plugins.knowledgebase.upgrade.KbSurveyUpgrade" public="true">
     	<interface>com.atlassian.sal.api.upgrade.PluginUpgradeTask</interface>
	</component>
	<component name="Annotated Event Listener" class="com.atlassian.confluence.plugins.knowledgebase.AnnotatedListener" key="annotatedListener" alias="annotatedListener"/>

    <xwork name="Knowledge Base Actions" key="kbactions">
        <package name="feedback" extends="default" namespace="/knowledgebase">
            <default-interceptor-ref name="defaultStack"/>
            <action name="evictsurveyresults" class="com.atlassian.confluence.plugins.knowledgebase.results.EvictSurveyResultsAction" >
                <result name="success" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/results/evictsurveyresults.vm</result>
            </action>
            <action name="doevictsurveyresults" class="com.atlassian.confluence.plugins.knowledgebase.results.EvictSurveyResultsAction" method="doRemove">
                <result name="success" type="redirect">${@com.atlassian.confluence.util.GeneralUtil@getPageUrl(page)}</result>
                <result name="cancel" type="redirect">${@com.atlassian.confluence.util.GeneralUtil@getPageUrl(page)}</result>
            </action>
             <action name="kbsearchresults" class="com.atlassian.confluence.plugins.knowledgebase.search.KbSearchResultsAction" method="execute">
                 <result name="input" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/search/kbsearchresults.vm</result>
                 <result name="success" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/search/kbsearchresults.vm</result>
                 <result name="error" type="redirect">/pages/pagenotfound.action</result>
             </action>
        </package>

        <package name="ajaxkb" extends="default" namespace="/plugins/kb/ajaxkb">
            <default-interceptor-ref name="defaultStack"/>
            <action name="submit" class="com.atlassian.confluence.plugins.knowledgebase.survey.SubmitSurveyAction" >
                 <result name="success" type="velocity-xml">/com/atlassian/confluence/plugins/knowledgebase/survey/ok.vm</result>  
            </action>
        </package>

        <package name="jsonkb" extends="admin" namespace="/jsonkb">
            <action name="kbsearch" class="com.atlassian.confluence.plugins.knowledgebase.search.KbSearchAction">
                 <result name="success" type="json"/>
            </action>
        </package>
        
        <package name="spaceadmin" extends="default" namespace="/knowledgebase">
            <default-interceptor-ref name="defaultStack"/>
            <action name="configuresurvey" class="com.atlassian.confluence.plugins.knowledgebase.config.ConfigureSurveyAction">
                <result name="input" type="velocity">/html/com/atlassian/confluence/plugins/knowledgebase/config/configkbsurvey.html.vm</result>
                <param name="RequireSecurityToken">true</param>
            </action>
            <action name="addquestion" class="com.atlassian.confluence.plugins.knowledgebase.config.AddFeedbackAction">
                <param name="RequireSecurityToken">true</param>
                <result name="input" type="redirect">/knowledgebase/configuresurvey.action?key=${key}</result>
                <result name="success" type="velocity">/html/com/atlassian/confluence/plugins/knowledgebase/config/configkbsurvey.html.vm</result>
                <result name="cancel" type="redirect">/spaces/spaceadmin.action?key=${key}</result>
            </action>
            <action name="deactivatequestion" class="com.atlassian.confluence.plugins.knowledgebase.config.DeactivateFeedbackQuestionAction" >
                <param name="RequireSecurityToken">true</param>
                <result name="success" type="velocity">/html/com/atlassian/confluence/plugins/knowledgebase/config/configkbsurvey.html.vm</result>
                <result name="input" type="redirect">/knowledgebase/configuresurvey.action?key=${key}</result>
            </action>
            <action name="kbresults" class="com.atlassian.confluence.plugins.knowledgebase.results.KbResultsAction" key="kbresults" >
                <result name="input" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/results/kbresults.vm</result>
                <result name="success" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/results/kbresults.vm</result>
            </action>
            <action name="kbunansweredpages" class="com.atlassian.confluence.plugins.knowledgebase.results.KbUnansweredPagesAction" key="kbunansweredpages" >
                <result name="input" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/results/kbunansweredpages.vm</result>
                <result name="success" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/results/kbunansweredpages.vm</result>
            </action>
            <action name="kbrollup" class="com.atlassian.confluence.plugins.knowledgebase.results.KbRollupAction" key="kbrollup" >
                <result name="input" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/results/rollup.vm</result>
                <result name="success" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/results/rollup.vm</result>
            </action>
            <action name="garesults" class="com.atlassian.confluence.plugins.knowledgebase.results.GaResults" key="garesults" >
                <result name="input" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/results/garesults.vm</result>
                <result name="success" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/results/garesults.vm</result>
            </action>
        </package>

        <package name="Knowledge Base Search Configuration" extends="admin" namespace="/admin/plugins/knowledgebase">
            <default-interceptor-ref name="defaultStack" />
            <action name="configure" class="com.atlassian.confluence.plugins.knowledgebase.ConfigureAction">
                <result name="success" type="redirect">/admin/plugins/knowledgebase/configure.action</result>
                <result name="input" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/configure.vm</result>
                <result name="error" type="velocity">/com/atlassian/confluence/plugins/knowledgebase/configure.vm</result>
            </action>
        </package>

    </xwork>

    <web-section key="knowledgebase" name="Survey Config" location="system.space.admin" weight="1000">
      <label key="knowledgebase.survey.space.config.web.section" />
      <condition class="com.atlassian.confluence.plugins.knowledgebase.SurveyCondition" />
    </web-section>

    <web-item key="configkbsurvey" name="Configure KB Survey" section="system.space.admin/knowledgebase" weight="1">
      <label key="knowledgebase.survey.space.config.web.item" />
      <link>/knowledgebase/configuresurvey.action?key=$space.key</link>
    </web-item>

    <web-item key="kbresults" name="KB Survey Results" section="system.space.admin/knowledgebase" weight="1">
      <label key="knowledgebase.survey.space.results.web.item" />
      <link>/knowledgebase/kbresults.action?key=$space.key</link>
    </web-item>

    <web-item key="evict_survey_data" name="Evict Survey Data Menu Item" section="system.content.action/secondary" weight="100">
        <condition class="com.atlassian.confluence.plugins.knowledgebase.SurveyCondition" />
        <description key="item.survey.feedback.results">Delete Survey Results</description>
           <label key="Evict Survey Feedback" />
           <link linkId="evict_survey_feedback">/knowledgebase/evictsurveyresults.action?pageId=$page.id</link>
    </web-item>
</atlassian-plugin>
