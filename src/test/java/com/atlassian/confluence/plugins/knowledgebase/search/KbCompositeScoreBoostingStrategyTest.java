package com.atlassian.confluence.plugins.knowledgebase.search;

import com.atlassian.confluence.plugins.knowledgebase.SurveySettingsManager;
import org.apache.lucene.index.IndexReader;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import java.io.IOException;


public class KbCompositeScoreBoostingStrategyTest extends MockObjectTestCase
{
    private Mock mockSettingsManager;
    private SurveySettingsManager settingsManager;

    private KbCompositeScoreBoostingStrategy strategy;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        mockSettingsManager = new Mock(SurveySettingsManager.class);
        settingsManager = (SurveySettingsManager) mockSettingsManager.proxy();

        mockSettingsManager.expects(atLeastOnce()).method("getBoostMaximum").withNoArguments().will(returnValue(100));
        mockSettingsManager.expects(once()).method("getBoostMinimum").withNoArguments().will(returnValue(40));
        mockSettingsManager.expects(once()).method("getBoostAmplifier").withNoArguments().will(returnValue(3));

        strategy = new KbCompositeScoreBoostingStrategy(settingsManager)
        {
            @Override
            String lookupCompositeScore(IndexReader reader, int doc, float score) throws IOException
            {
                return "1.0";
            }
        };
    }

    @Override
    protected void tearDown() throws Exception
    {
        settingsManager = null;
        super.tearDown();
    }

    public void testBoost() throws Exception
    {
        assertEquals(6.0F, strategy.boost(null, 1, 1.5F), 0.001F);
    }
}
