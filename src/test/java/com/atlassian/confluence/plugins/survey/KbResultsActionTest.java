package com.atlassian.confluence.plugins.survey;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugins.knowledgebase.SurveyFeedbackManager;
import com.atlassian.confluence.plugins.knowledgebase.config.SurveyConfig;
import com.atlassian.confluence.plugins.knowledgebase.results.KbResultsAction;
import com.atlassian.confluence.plugins.knowledgebase.results.SurveyResult;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.opensymphony.webwork.ServletActionContext;
import org.jmock.Mock;
import org.jmock.cglib.MockObjectTestCase;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


public class KbResultsActionTest extends MockObjectTestCase
{

    private Settings globalSettings;

    private Mock mockSpaceManager;
    private SpaceManager spaceManager;

    private Mock mockPageManager;
    private PageManager pageManager;

    private Mock mockSurveyFeedbackManager;
    private SurveyFeedbackManager surveyFeedbackManager;

    private Mock mockHttpServletRequest;
    private HttpServletRequest httpServletRequest;

    private Mock mockSettingsManager;
    private SettingsManager settingsManager;

    private KbResultsAction kbResultsAction;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockSpaceManager = new Mock(SpaceManager.class);
        spaceManager = (SpaceManager) mockSpaceManager.proxy();

        mockPageManager = new Mock(PageManager.class);
        pageManager = (PageManager) mockPageManager.proxy();

        mockSurveyFeedbackManager = new Mock(SurveyFeedbackManager.class);
        surveyFeedbackManager = (SurveyFeedbackManager) mockSurveyFeedbackManager.proxy();

        mockHttpServletRequest = new Mock(HttpServletRequest.class);
        httpServletRequest = (HttpServletRequest) mockHttpServletRequest.proxy();

        mockSettingsManager = new Mock(SettingsManager.class);
        settingsManager = (SettingsManager) mockSettingsManager.proxy();

        ServletActionContext.setRequest(httpServletRequest);

        kbResultsAction = new KbResultsAction();
        injectKBResultsActionDeps();

    }

    private void injectKBResultsActionDeps()
    {
        kbResultsAction.setSpaceManager(spaceManager);
        kbResultsAction.setPageManager(pageManager);
        kbResultsAction.setSurveyFeedbackManager(surveyFeedbackManager);
        kbResultsAction.setSettingsManager(settingsManager);

    }

    public void testOneSpaceOnePageResult()
    {
        final Space spaceKB = new Space("KB");
        final Page pageTst = new Page();
        pageTst.setTitle("Article");
        Settings settings = new Settings();
        SurveyConfig surveyConfig = new SurveyConfig();
        List<SurveyResult> pagedResults = new ArrayList<SurveyResult>();
        SurveyResult result = new SurveyResult();

        settings.setBaseUrl("http://localhost:1990/confluence");
        kbResultsAction.setSpace(spaceKB);

        result.setCompositeScore("2");
        pagedResults.add(result);


        mockSettingsManager.expects(once()).method("getGlobalSettings").withNoArguments().will(returnValue(settings));
        mockSurveyFeedbackManager.expects(once()).method("getSurveyConfig").with(eq(spaceKB.getDescription()));
        mockSurveyFeedbackManager.expects(once()).method("getPagedResults").with(eq("composite_desc"), eq("KB"), eq(0), eq(15)).will(returnValue(pagedResults));
        mockSurveyFeedbackManager.expects(once()).method("getNumberOfSurveyedPages").with(eq("KB")).will(returnValue(1));

        try
        {
            assertEquals("returns SUCCESS","success", kbResultsAction.execute());
        }
        catch (Exception e)
        {
            fail("Caught exception " + e.getMessage());
        }
        
    }

}
