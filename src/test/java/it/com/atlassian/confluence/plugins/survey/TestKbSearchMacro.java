package it.com.atlassian.confluence.plugins.survey;

public class TestKbSearchMacro extends AbstractConfluencePluginWebTestCaseBase
{
    public void testKbSearchMacroProducesResultsFromDefaultSpace() throws InterruptedException
    {
        _configureSpace();
        _addPage(getDefaultSpaceKey(), "title", "pagecontent");

        //flush the index to make sure the search works
        gotoPage("/admin/search-indexes.action");

        //Rebuild the index to make sure the search works
        clickButtonWithText("Rebuild");
        Thread.sleep(3000);

        gotoPage("/knowledgebase/kbsearchresults.action?queryString=kb&where=" + getDefaultSpaceKey() + "&type=pages&type=blogposts&type=attachments");

        //unfortunately search results are not showing up. The index isn't flushing?
        assertTextPresent("Searching Knowledge Base");
    }
}
