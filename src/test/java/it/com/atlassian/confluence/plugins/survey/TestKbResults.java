package it.com.atlassian.confluence.plugins.survey;

public class TestKbResults extends AbstractConfluencePluginWebTestCaseBase
{

    public void test20YesAnswersRendersInPagesWithResultsReport()
    {
        _configureSpace();
        _addQuestion("question1");
        gotoPage("/display/"+ getDefaultSpaceKey() + "/KBarticle");
        assertTextPresent("question1");

        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb0&value=Yes");
        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb1&value=No");

        for (int i = 0; i < 19; i++)
        {
            gotoPage("/display/" + getDefaultSpaceKey() + "/KBarticle");
            gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb0&value=Yes");
            gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb1&value=No");
        }

        gotoPage("/knowledgebase/kbresults.action?key=" + getDefaultSpaceKey());
        assertTextPresent("question1");

        String[] resultRow = {"KBarticle", "20",  "40", "100% (20/20)",  "0% (0/20)"};
        for (String s : resultRow)
            assertTextPresent(s);

        //hope to get ajax response working - but does not appear in web test
        //assertTextPresent("40");
    }

    public void test20NoAnswersRendersInPagesWithResultsReport()
    {
        _configureSpace();
        _addQuestion("question1");
        gotoPage("/display/"+ getDefaultSpaceKey() + "/KBarticle");
        assertTextPresent("question1");

        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb0&value=No");
        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb1&value=No");

        for (int i = 0; i < 19; i++)
        {
            logout();
            loginAsAdmin();
            gotoPage("/display/" + getDefaultSpaceKey() + "/KBarticle");
            gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb0&value=No");
            gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb1&value=No");
        }

        gotoPage("/knowledgebase/kbresults.action?key=" + getDefaultSpaceKey());
        assertTextPresent("question1");

        String[] resultRow = {"KBarticle", "20",  "-20", "0% (0/20)",  "0% (0/20)"};
        for (String s : resultRow)
            assertTextPresent(s);

        // hope to get ajax response working - but does not appear in web test
        //assertTextPresent("40");
    }

    public void testPagesWithResultsShowsColumns()
    {
        _configureSpace();
        gotoPage("/knowledgebase/kbresults.action?key=" + getDefaultSpaceKey());
        assertTextPresent("Total Ballots");
        assertTextPresent("Composite Score");
        assertTextPresent("No Questions Configured");
        assertTextPresent("Knowledge Base");     //name of space
    }

    public void testPagesWithResultsReportCanSortByCompositeScore()
    {
        _configureSpace();
        //Add 25 pages
        for (int i = 0; i < 25; i++)
        {
            _addPage(getDefaultSpaceKey(),"page"+i,"{kbsurvey}");
            //increment each pages' composite scores so the composite scores are all different
            for (int j = 0; j <=i ; j++)
            {
                gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("page"+i) + "&questionId=kb0&value=Yes");
            }
        }
        gotoPage("/knowledgebase/kbresults.action?key=" + getDefaultSpaceKey());
        assertTextPresent("page24"); //this should have the highest composite score

        gotoPage("/knowledgebase/kbresults.action?key=" + getDefaultSpaceKey()+"&sort=composite_asc"); //sort it
        assertTextPresent("page0"); //this should have the lowest composite score
    }

   public void testPagesWithResultsReportCanSortByTotalResults()
    {
        _configureSpace();
        //Add 25 pages
        for (int i = 0; i < 25; i++)
        {
            _addPage(getDefaultSpaceKey(),"page"+i,"{kbsurvey}");
            //increment each pages' total results so the composite scores are all different
            for (int j = 0; j <=i ; j++)
            {
                gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("page"+i) + "&questionId=kb0&value=Yes");
            }
        }
        gotoPage("/knowledgebase/kbresults.action?key=" + getDefaultSpaceKey());
        assertTextPresent("page24"); //this should have the highest total ballots

        gotoPage("/knowledgebase/kbresults.action?key=" + getDefaultSpaceKey()+"&sort=total_asc"); //sort it
        assertTextPresent("page0"); //this should have the lowest total ballots
    }


    public void testPagesWithoutResultsReport()
    {
        _configureSpace();
        _addQuestion("question1");
        gotoPage("/display/" + getDefaultSpaceKey() + "/KBarticle");
        assertTextPresent("question1");

        _addPage(getDefaultSpaceKey(), "No Vote", "bla bla");

        // vote
        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb0&value=Yes");
        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb1&value=No");

        gotoPage("/knowledgebase/kbresults.action?key=" + getDefaultSpaceKey());
        assertTextPresent("question1");

        gotoPage("/knowledgebase/kbunansweredpages.action?key=" + getDefaultSpaceKey());
        assertTextPresent("No Vote");
    }

    public void testPagesWithoutResultsReportCanSortCorrectly()
    {
         _configureSpace();
        //Add 25 pages
        for (int i = 0; i < 25; i++)
            _addPage(getDefaultSpaceKey(),"page"+i,"{kbsurvey}");


        gotoPage("knowledgebase/kbunansweredpages.action?key=" + getDefaultSpaceKey());
        assertTextPresent("page1");
        
        //fill out surveys for 5 pages
        for (int j = 0; j <=10 ; j++)
        {
           gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("page"+j) + "&questionId=kb0&value=Yes");
        }
        gotoPage("/knowledgebase/kbunansweredpages.action?key=" + getDefaultSpaceKey());
        assertTextPresent("page11");
    }

    public void testRollupReportShowsCorrectColumnsWithNoSurveyConfigured()
    {
        _configureSpace();

        gotoPage("/knowledgebase/kbrollup.action?key="  + getDefaultSpaceKey());
        assertTextPresent("No Questions Configured");
    }

    public void testRollupReportShowsCorrectColumnsWithSurveyConfigured()
    {
        _configureSpace();
        _addQuestion("question1");
        gotoPage("/knowledgebase/kbrollup.action?key="  + getDefaultSpaceKey());
        assertTextPresent("Number of Pages in Space");
        assertTextPresent("Total Ballots Cast");
        assertTextPresent("question1");
    }

    public void testRollupReportShowsData()
    {
         _configureSpace();
        //Add 25 pages
        for (int i = 0; i < 25; i++)
            _addPage(getDefaultSpaceKey(),"page"+i,"{kbsurvey}");

        for (int j = 0; j < 10 ; j++)
        {
           gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("page"+j) + "&questionId=kb0&value=Yes");
        }
        gotoPage("/knowledgebase/kbrollup.action?key="  + getDefaultSpaceKey());
        assertTextPresent("27"); //2 pages added on setup, plus 25 more for 27 total pages in space
        assertTextPresent("10"); //number of surveys

    }
    
    public void testGaReportShowsData()
    {
         _configureSpace();
        // Add 1 page
        _addPage(getDefaultSpaceKey(),"Test Page","{kbsurvey}");
        // Add votes with GA information
        for (int i = 0; i < 10 ; i++)
        {
        	// Source and medium is the same in all answers, content changes
        	gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("Test Page") + "&questionId=" + getDefaultSpaceKey() + "0&value=Yes&utmSource=SOURCE&utmMedium=MEDIUM&utmContent=C" + i);        
        }
        // Add vote without GA information
        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("Test Page") + "&questionId=" + getDefaultSpaceKey() + "0&value=Yes");
        
        // Check source info
        gotoPage("/knowledgebase/garesults.action?key="  + getDefaultSpaceKey());
        assertTextPresent("SOURCE");
        assertTextPresent("10"); //number of surveys
        assertTextPresent("Undefined"); // For the row without GA information
        // Check medium info
        gotoPage("/knowledgebase/garesults.action?key="  + getDefaultSpaceKey() + "&group=medium");
        assertTextPresent("MEDIUM");
        assertTextPresent("10"); //number of surveys
        assertTextPresent("Undefined"); // For the row without GA information
     // Check content info
        gotoPage("/knowledgebase/garesults.action?key="  + getDefaultSpaceKey()  + "&group=content");
        for (int i = 0; i < 10 ; i++) // There should be 10 different contents
        	assertTextPresent("C" + i);
        assertTextPresent("Undefined"); // For the row without GA information
    }
}
