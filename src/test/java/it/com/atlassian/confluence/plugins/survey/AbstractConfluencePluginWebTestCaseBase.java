package it.com.atlassian.confluence.plugins.survey;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;

import java.util.*;


public class AbstractConfluencePluginWebTestCaseBase extends AbstractConfluencePluginWebTestCase
{
    private static final String TEST_SPACE = "kb";
    private List<String> spaceList = new ArrayList<String>();
    private Map<String, Long> pageMap = new HashMap<String, Long>();

    public AbstractConfluencePluginWebTestCaseBase()
    {
        super();
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        spaceList.add(TEST_SPACE);

        //Create the initial space
        SpaceHelper spaceHelper = getSpaceHelper();
        spaceHelper.setKey(getDefaultSpaceKey());
        spaceHelper.setName("Knowledge Base");
        spaceHelper.setDescription("kb test space description");
        spaceHelper.setHomePageId(55555);
        assertTrue(spaceHelper.create());
        pageMap.put("KBarticle", createPage(getDefaultSpaceKey(), "KBarticle", "{kbsurvey}"));
    }

    protected long createPage(String spaceKey, String title, String content)
    {
        PageHelper helper = getPageHelper();

        helper.setSpaceKey(spaceKey);
        helper.setParentId(0);
        helper.setTitle(title);
        helper.setContent(content);
        helper.setCreationDate(new Date());
        //helper.setLabels(labels);
        assertTrue(helper.create());

        // return the generated id for the new page
        return helper.getId();
    }

    protected long createPage(String spaceKey, String title, String content, List<String> labels)
    {
        PageHelper helper = getPageHelper();

        helper.setSpaceKey(spaceKey);
        helper.setParentId(0);
        helper.setTitle(title);
        helper.setContent(content);
        helper.setCreationDate(new Date());
        helper.setLabels(labels);
        assertTrue(helper.create());

        // return the generated id for the new page
        return helper.getId();
    }

    @Override
    protected void tearDown() throws Exception {
        for(String spaceKey : spaceList)
        {
            deleteSpace(spaceKey);
        }
        super.tearDown();
    }

    protected String getDefaultSpaceKey()
    {
        assertTrue(spaceList.size() > 0);
        return spaceList.get(0);
    }

    protected void deleteSpace(String spaceKey)
    {
        SpaceHelper spaceHelper = getSpaceHelper(spaceKey);
        assertTrue(spaceHelper.delete());
    }

    protected void _addPage(String spaceKey, String title, String content)
    {
        assertTrue(spaceList.contains(spaceKey));
        pageMap.put(title, createPage(spaceKey, title, content));
    }

    protected void _addPage(String spaceKey, String title, String content, List<String> labels)
    {
        assertTrue(spaceList.contains(spaceKey));
        pageMap.put(title, createPage(spaceKey, title, content, labels));
    }

    protected void _configureSpace()
    {
        _configureSpace_5();
    }
    private void _configureSpace_4()
    {
        assertTrue(spaceList.contains(TEST_SPACE));
        gotoPage("/display/"+ TEST_SPACE + "/KBarticle");
        assertTextPresent("KBarticle");
        clickLink("space-admin-link");
        assertTextPresent("Configure Survey");
        clickLinkWithText("Configure Survey");
        //clickLink("Configure Survey");
        //gotoPage("/knowledgebase/configuresurvey.action?key=kb");
    }
    private void _configureSpace_5()
    {
        assertTrue(spaceList.contains(TEST_SPACE));
        gotoPage("/spaces/viewspacesummary.action?key=" + TEST_SPACE);
        assertTextPresent("Knowledge Base Survey");
        clickLinkWithText("Knowledge Base Survey");
        assertTextPresent("Survey Configuration");
        //clickLink("Configure Survey");
        //gotoPage("/knowledgebase/configuresurvey.action?key=kb");
    }

    protected void _addQuestion(String q)
    {
        setWorkingForm("configurequestions");
        setTextField("additionalQuestion", q);
        clickButton("add");
    }

    protected long getPageId (String title)
    {
        assertTrue(pageMap.containsKey(title));
        return pageMap.get( title );
    }

}
