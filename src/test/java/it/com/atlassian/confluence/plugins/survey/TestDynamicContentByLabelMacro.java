package it.com.atlassian.confluence.plugins.survey;

import java.util.LinkedList;
import java.util.List;


public class TestDynamicContentByLabelMacro extends AbstractConfluencePluginWebTestCaseBase
{
    public void testDynamicContentByLabelMacro()
    {
        _configureSpace();
        List<String> labels = new LinkedList<String>();
        labels.add("label1");
        _addPage(getDefaultSpaceKey(), "dynamicContentTestPage", "{dynamiccontentbylabel}", labels);
        _addPage(getDefaultSpaceKey(), "dynamicContentTestPage2", "{dynamiccontentbylabel}", labels);
        gotoPage("/display/" + getDefaultSpaceKey() + "/dynamicContentTestPage");

        //not showing labeled content as it needs to be indexed
        //so we'll just show the title for now
        assertTextPresent("Related Content with Label 'label1'");
    }
}
