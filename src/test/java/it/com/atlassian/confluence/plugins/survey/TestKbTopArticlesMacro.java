package it.com.atlassian.confluence.plugins.survey;

public class TestKbTopArticlesMacro extends AbstractConfluencePluginWebTestCaseBase
{
    public void testTopArticles()
    {
        _configureSpace();
        _addQuestion("question1");
        gotoPage("/display/" + getDefaultSpaceKey() + "/KBarticle");        
        assertTextPresent("question1");

        _addPage(getDefaultSpaceKey(), "Other Article", "{kbsurvey}");
        _addPage(getDefaultSpaceKey(), "No Vote", "bla bla");
        _addPage(getDefaultSpaceKey(), "TopArticles", "{kbtoparticles}" );

        // vote
        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb0&value=Yes");
        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb1&value=No");

        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("Other Article") + "&questionId=kb0&value=No");
        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("Other Article") + "&questionId=kb1&value=No");



        gotoPage("/display/"+ getDefaultSpaceKey() + "/TopArticles");
        assertTextPresent("KBarticle");
        assertTextPresent("2");

        assertTextPresent("Other Article");
        assertTextPresent("-1");
    }
}
