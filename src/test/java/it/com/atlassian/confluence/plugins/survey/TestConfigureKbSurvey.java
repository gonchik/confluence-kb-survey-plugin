package it.com.atlassian.confluence.plugins.survey;


public class TestConfigureKbSurvey extends AbstractConfluencePluginWebTestCaseBase
{

    public void testPageAppears()
    {
        gotoPage("/display/" + getDefaultSpaceKey() + "/KBarticle");
        assertTextPresent("KBarticle");
    }

    public void testConfigContainsWasThisHelpfulText()
    {
        _configureSpace();
        _addQuestion("question1");
        assertTextPresent("Survey Configuration");
        assertFormElementMatch("primaryQuestion", "Was this helpful?"); //the default question is "was this helpful"
    }

    public void testConfigCanDeactivateAndReactivateQuestion()
    {
        _configureSpace();
        _addQuestion("question1");

        assertFormElementMatch("kb1", "question1");

        // add another question
        _addQuestion("question2");
        assertFormElementMatch("kb2", "question2");
        clickLink("deactivatekb2");
        assertLinkNotPresent("deactivatekb2");
        assertLinkPresent("activatekb2");
        clickLink("activatekb2");
        assertLinkPresent("deactivatekb2");

        assertFormElementMatch("kb1", "question1");
        assertFormElementMatch("kb2", "question2");
    }

    public void testConfigCanEditQuestion()
    {
        _configureSpace();
        _addQuestion("question1");
        assertFormElementMatch("kb1", "question1");
        _addQuestion("question2");

        setWorkingForm("configurequestions");
        setTextField("kb2", "question2");

        clickButton("add");

        assertFormElementMatch("kb1", "question1");
        assertFormElementMatch("kb2", "question2");
    }

    public void testCanEditResponse()
    {
        _configureSpace();
        setWorkingForm("configurequestions");
        assertEquals("Thanks!", contentForTextare("response"));

        setWorkingForm("configurequestions");
        final String newResponse = "Response text";
        setTextField("response", newResponse);
        clickButton("add");

        assertEquals(newResponse, contentForTextare("response"));
    }

    private String contentForTextare(final String elementName)
    {
        return getTestingEngine().getElementTextByXPath("//textarea[@name='" + elementName + "']");
    }
}
