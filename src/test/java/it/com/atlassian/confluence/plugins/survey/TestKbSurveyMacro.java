package it.com.atlassian.confluence.plugins.survey;


public class TestKbSurveyMacro extends AbstractConfluencePluginWebTestCaseBase
{
    public void testSurveyQuestionRenders()
    {
        _configureSpace();
        _addQuestion("question1");
        gotoPage("/display/"+ getDefaultSpaceKey() + "/KBarticle");
        assertTextPresent("helpful"); //default question
        assertTextPresent("question1");
    }

    public void testSurveyAnswerRenders()
    {
        _configureSpace();
        _addQuestion("question1");

        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb0&value=Yes");
        gotoPage("/plugins/kb/ajaxkb/submit.action?pageId=" + getPageId("KBarticle") + "&questionId=kb1&value=Yes");

        gotoPage("/display/"+ getDefaultSpaceKey() + "/KBarticle");
        // hope to see the ajax response - but does not appear in web test, therefore just testing that the questions are gone
        assertTextNotPresent("helpful");
        assertTextNotPresent("question1");
        //assertTextPresent("Thanks"); //default response
    }
}
